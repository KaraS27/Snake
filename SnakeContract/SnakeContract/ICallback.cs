﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Drawing;

namespace SnakeContract
{
    public interface ICallback
    {
        //It's from the host part
        [OperationContract(IsOneWay = true)]
        void Update(string name, int number);

        [OperationContract(IsOneWay = true)]
        void AlreadyOnline();

        [OperationContract(IsOneWay = true)]
        void Full(int Nmax);

        [OperationContract(IsOneWay = true)]
        void ReceiveLocation(Point[][] locations, Point[] meals);

        [OperationContract(IsOneWay = true)]
        void SomeoneLeft(string name);

        [OperationContract(IsOneWay = true)]
        void SendMess(string MessSender, string MessServer);

        [OperationContract(IsOneWay = true)]
        void IndexOfThePlayer(int p, Point[] obstacles);

    }

}
