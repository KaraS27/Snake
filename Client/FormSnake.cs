﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ServiceModel;
using SnakeContract;
using SnakeService;

namespace Client
{
    public partial class FormSnake : Form
    {
        private int score = 0;
        private Keys direction;
        private Keys arrow;
        private Point lastsegment;
        private Snake snake;
        private Meal meal;
        private Graphics Bitmatgraph;
        private Graphics ScreenGraph;
        private Bitmap offscreenbitmap;

        public FormSnake()
        {
            InitializeComponent();
            direction = Keys.Left;
            arrow = direction;
            offscreenbitmap = new Bitmap(500, 500);
            snake = new Snake();
            meal = new Meal();
            lastsegment = snake.Location[snake.Length - 1];

            startGame();
        }


        private void timer1_Tick_1(object sender, EventArgs e)
        {

            lastsegment = snake.Location[snake.Length - 1];
            if (((((arrow == Keys.Left) && (direction != Keys.Right)) || (arrow == Keys.Right) && (direction != Keys.Left)) || (arrow == Keys.Up) && (direction != Keys.Down)) || (arrow == Keys.Down) && (direction != Keys.Up)) direction = arrow;

            switch (direction)
            {
                case Keys.Left:
                    snake.Left();
                    break;
                case Keys.Right:
                    snake.Right();
                    break;
                case Keys.Up:
                    snake.Up();
                    break;
                case Keys.Down:
                    snake.Down();
                    break;
            }

            try
            {
                Singleton.CommObj.SendLocation(snake.Location, meal.Location);
            }
            catch
            {


            }
        }


        protected override void OnKeyDown(KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.Left) || (e.KeyCode == Keys.Right) || (e.KeyCode == Keys.Down) || (e.KeyCode == Keys.Up))
                arrow = e.KeyCode;
        }

        protected bool IsInputKeys(Keys keyData)
        {
            return true;
        }


        

        private void creatMeal()
        {
            bool place;
            do
            {
                meal.SetFoodLocation();
                place = false;
                for (int i = 0; i < snake.Length; i++)
                {
                    if (meal.Location == snake.Location[i])
                    {
                        place = true;
                        break;
                    }
                }
            }
            while (place == true);
        }

        private void startGame()
        {
            snake.Reset();
            creatMeal();
            direction = Keys.Left;
            timer1.Interval = 150;
            scorelabel.Text = "0";
            score = 0;
            Bitmatgraph = Graphics.FromImage(offscreenbitmap);
            ScreenGraph = playingarea.CreateGraphics();
            timer1.Enabled = true;
        }

        private void update(Point[][]snakes, Point[] meals)
        {
            for (int i = 0; i < snakes.Length; i++)
            {
                if (snakes[i] == null) continue;
                for (int j = 0; j < meals.Length; j++)
                {
                    if (meals[j] == null) continue;
                    if (snakes[i][0] == meals[j])
                    {
                        if (i== Singleton.IndexOfThePlayer -1)
                        {
                            snake.incLength();
                            score += 10;
                            scorelabel.Text = score.ToString();
                        }
                        if (j == Singleton.IndexOfThePlayer - 1) creatMeal();
                    }
                }
            }
        }

        private void gameOver()
        {
            timer1.Enabled = false;
            Bitmatgraph.Dispose();
            ScreenGraph.Dispose();

            String text1 = "Your Score: " + score + "\n Length : " + snake.Length + "\nPlay Again?";
            if (MessageBox.Show(text1, "Game Over", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                startGame();
            else
                Close();

        }

        internal void UpdateLocation(Point[][] locations,Point [] meals)
        {
            Bitmatgraph.Clear(Color.White);
            for (int i = 0; i < meals.Length; i++)
            {
                if (meals[i] == null) continue;
                if (meals[i].X < 0 || meals[i].Y < 0) continue;
                Bitmatgraph.FillEllipse(new SolidBrush(Color.Blue), (meals[i].X * 10), (meals[i].Y * 10), 10, 10);

            }
            bool gameover = false;
            for (int j = 0; j < locations.Length; j++)
            {
                Color r = (j == 0) ? Color.Red : Color.Blue;
                if (locations[j]== null) continue;
                for (int i = 0; i < locations[j].Length; i++)
                {
                    if (i == 0)
                        Bitmatgraph.FillRectangle(new SolidBrush(r), (locations[j][i].X * 10), (locations[j][i].Y * 10), 10, 10);
                    else Bitmatgraph.FillRectangle(new SolidBrush(Color.Black), (locations[j][i].X * 10), (locations[j][i].Y * 10), 10, 10);
                    if ((locations[j][i] == locations[j][0]) && (i > 0))
                        gameover = true;
                }

                // wtfff
                if (Singleton.Obstacles!= null)
                {
                    for (int i = 0; i < Singleton.Obstacles.Length; i++)
                    {
                        if ((locations[j][0] == Singleton.Obstacles[i]))
                            gameover = true;
                    }
                }
            }
            if (Singleton.Obstacles != null)
            {
                for (int i = 0; i < Singleton.Obstacles.Length; i++)
                {
                    Bitmatgraph.FillRectangle(new SolidBrush(Color.Green), (Singleton.Obstacles[i].X * 10), (Singleton.Obstacles[i].Y * 10), 10, 10);
                }
            }
            try
            {
                ScreenGraph.DrawImage(offscreenbitmap, 0, 0);
            }
            catch 
            {}

            update(locations, meals);
            if (gameover == true)
            {
                gameOver();
            }

        }

        private void FormSnake_FormClosing(object sender , FormClosedEventArgs e)
        {
            timer1.Stop();
        }

        private void FormSnake_Load(object sender, EventArgs e)
        {

        }
        
        private void aboutMeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Rakotondrajoa safidy");
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //startGame();
        }
    }   
}
