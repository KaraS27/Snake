﻿namespace Client
{
    partial class FrmConnect
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPlayer = new System.Windows.Forms.TextBox();
            this.txtHost = new System.Windows.Forms.TextBox();
            this.bConnect = new System.Windows.Forms.Button();
            this.bDeconnect = new System.Windows.Forms.Button();
            this.bPlay = new System.Windows.Forms.Button();
            this.textMessages = new System.Windows.Forms.TextBox();
            this.bDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Player Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Server Name/IP Adress";
            // 
            // txtPlayer
            // 
            this.txtPlayer.Location = new System.Drawing.Point(197, 9);
            this.txtPlayer.Name = "txtPlayer";
            this.txtPlayer.Size = new System.Drawing.Size(123, 20);
            this.txtPlayer.TabIndex = 2;
            // 
            // txtHost
            // 
            this.txtHost.Location = new System.Drawing.Point(197, 38);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(123, 20);
            this.txtHost.TabIndex = 3;
            this.txtHost.Text = "localhost";
            // 
            // bConnect
            // 
            this.bConnect.Location = new System.Drawing.Point(27, 78);
            this.bConnect.Name = "bConnect";
            this.bConnect.Size = new System.Drawing.Size(104, 43);
            this.bConnect.TabIndex = 4;
            this.bConnect.Text = "Connect";
            this.bConnect.UseVisualStyleBackColor = true;
            this.bConnect.Click += new System.EventHandler(this.bConnect_Click);
            // 
            // bDeconnect
            // 
            this.bDeconnect.Location = new System.Drawing.Point(365, 78);
            this.bDeconnect.Name = "bDeconnect";
            this.bDeconnect.Size = new System.Drawing.Size(104, 43);
            this.bDeconnect.TabIndex = 5;
            this.bDeconnect.Text = "disconnect";
            this.bDeconnect.UseVisualStyleBackColor = true;
            this.bDeconnect.Click += new System.EventHandler(this.bDeconnect_Click);
            // 
            // bPlay
            // 
            this.bPlay.Location = new System.Drawing.Point(197, 78);
            this.bPlay.Name = "bPlay";
            this.bPlay.Size = new System.Drawing.Size(104, 43);
            this.bPlay.TabIndex = 6;
            this.bPlay.Text = "Play";
            this.bPlay.UseVisualStyleBackColor = true;
            this.bPlay.Click += new System.EventHandler(this.bPlay_Click);
            // 
            // textMessages
            // 
            this.textMessages.Location = new System.Drawing.Point(12, 127);
            this.textMessages.Multiline = true;
            this.textMessages.Name = "textMessages";
            this.textMessages.ReadOnly = true;
            this.textMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textMessages.Size = new System.Drawing.Size(326, 50);
            this.textMessages.TabIndex = 10;
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(365, 127);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(104, 43);
            this.bDelete.TabIndex = 11;
            this.bDelete.Text = "Delete";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // FrmConnect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 187);
            this.Controls.Add(this.bDelete);
            this.Controls.Add(this.textMessages);
            this.Controls.Add(this.bPlay);
            this.Controls.Add(this.bDeconnect);
            this.Controls.Add(this.bConnect);
            this.Controls.Add(this.txtHost);
            this.Controls.Add(this.txtPlayer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConnect";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPlayer;
        private System.Windows.Forms.TextBox txtHost;
        private System.Windows.Forms.Button bConnect;
        private System.Windows.Forms.Button bDeconnect;
        private System.Windows.Forms.Button bPlay;
        private System.Windows.Forms.TextBox textMessages;
        private System.Windows.Forms.Button bDelete;
    }
}

