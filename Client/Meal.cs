﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Client
{
    public class Meal
    {
        private Point location;
        public Point Location
        {
            get
            {
                return location;
            }
        }

        public Meal()
        {
            location = new Point();
        }

        public void SetFoodLocation()
        {
            Random rand = new Random();
            bool test = true;
            do
            {
                location = new Point(rand.Next(0, 25), rand.Next(0, 25));
                test = false;
                if (Singleton.Obstacles != null)
                {
                    for (int i = 0; i < Singleton.Obstacles.Length; i++)
                    {
                        if (location == Singleton.Obstacles[i])
                        {
                            test = true;
                            break;
                        }
                    }
                }
            } while (test);
        }
    }
}
