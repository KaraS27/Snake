﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;
using SnakeContract;

namespace Client
{
    public partial class FrmConnect : Form,ICallback
    {
        FormSnake SnakeForm;
        public FrmConnect()
        {
            InitializeComponent();
        }

        private void bConnect_Click(object sender, EventArgs e)
        {
            if (txtPlayer.Text =="" || txtHost.Text=="")
            {
                MessageBox.Show("Please fill all the fields");
               
            }
            string name = txtPlayer.Text;
            string host = string.Format("net.tcp://{0}:8888/MySnakeGame", txtHost.Text);
            try
            {
                Singleton.Comm = new DuplexChannelFactory<ISnakeContract>(new InstanceContext(this),
                    new NetTcpBinding(),
                    host);
                Singleton.CommObj = Singleton.Comm.CreateChannel();
                Singleton.CommObj.Connect(name);
            }
            catch 
            {
                MessageBox.Show("Connexion Error....", "Networking", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            bConnect.Enabled = false;
            bDeconnect.Enabled = true;
            //bSend.Enabled = true;
            bPlay.Enabled = true;
        }

        private void bDeconnect_Click(object sender, EventArgs e)
        {
            try
            {
                Singleton.CommObj.Deconnect();
                Singleton.Comm.Close();
            }
            catch 
            {
                MessageBox.Show("NEtworking Error");
            }
            bConnect.Enabled = true;
            bDeconnect.Enabled = false;
            //bSend.Enabled = false;
            bPlay.Enabled = false;
        }

        private void bPlay_Click(object sender, EventArgs e)
        {
            SnakeForm = new FormSnake();
            Hide();
            SnakeForm.ShowDialog();
            Show();
        }

        /*private void bSend_Click(object sender, EventArgs e)
        {
            if (textMessage.Text == "") return;
            string message = textMessage.Text;
            textMessage.Text = "";
            try
            {
                Singleton.CommObj.SendMessage(message);
            }
            catch 
            {
                MessageBox.Show("MEssage unsent Network error", "Unsent Message", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            
        }*/

        private void bDelete_Click(object sender, EventArgs e)
        {
            textMessages.Text = "";
        }


        //from server
        public void AlreadyOnline()
        {
            MessageBox.Show("Lol", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void SendMess(string MessSender, string MessServer)
        {
            textMessages.Text += string.Format("{0}{1}\t> {2}", Environment.NewLine, MessSender, MessServer);

        }

        public void Update(string name, int number)
        {
            string res = string.Format("\r\n {0} Is Online Now \r\n", name);
            textMessages.Text += res;
        }

        public void SomeoneLeft(string name)
        {
            string res = string.Format("\r\n {0} has Logged Out  \r\n", name);
            textMessages.Text += res;
        }

        public void Full(int Max)
        {
            MessageBox.Show(string.Format("Max number reached {0}", Max));
            bConnect.Enabled = true;
            bConnect.Enabled = false;
            //bSend.Enabled = false;
            textMessages.Text = "";
        }

        public void ReceiveLocation(Point[][] locations, Point[] meals)
        {
            if (SnakeForm != null)
            {
                SnakeForm.UpdateLocation(locations, meals);
            }
        }

        public void IndexOfThePlayer(int p, Point[] obstacles)
        {
            Singleton.IndexOfThePlayer = p;
            Singleton.Obstacles = obstacles;
        }
    }
}
