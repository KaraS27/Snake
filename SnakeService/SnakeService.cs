﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ServiceModel;
using SnakeContract;
using System.Drawing;

namespace SnakeService
{
    [ServiceBehavior (InstanceContextMode =InstanceContextMode.Single,ConcurrencyMode =ConcurrencyMode.Reentrant)]
    public class SnakeService:SnakeContract.ISnakeContract
    {
        private List<ICallback> ConnectedClients = null;
        private List<string> Names = null;
        private const int max = 2;
        private static int count = 0;

        private Point[] meals = null;
        private Point[][] locations = null;

        private Point[] obstacles = new Point[] {
            new Point(10,10),
            new Point(11,10),
            new Point(12,10),
        };

        public SnakeService()
        {
            locations = new Point[max][];
            meals = new Point[max];
            for (int i = 0; i < meals.Length; i++)
            {
                meals[i] = new Point() { X = -10, Y = -10 };
            }
            ConnectedClients = new List<ICallback>();
            Names = new List<string>();
        }

        public void Connect(string name)
        {
            ICallback callback = OperationContext.Current.GetCallbackChannel<ICallback>();
            if (ConnectedClients.Contains(callback))
            {
                callback.AlreadyOnline();
            }
            else
            {
                if (ConnectedClients.Count >= max)
                {
                    callback.Full(max);
                }
                else
                {
                    foreach (var item in ConnectedClients)
                    {
                        item.Update(name, ConnectedClients.Count);
                    }

                    ConnectedClients.Add(callback);
                    Names.Add(name);
                    callback.IndexOfThePlayer(ConnectedClients.Count, obstacles);
                }
            }
        }

        public void Deconnect()
        {
            ICallback callback = OperationContext.Current.GetCallbackChannel<ICallback>();
            int NameIndex = ConnectedClients.IndexOf(callback);
            ConnectedClients.RemoveAt(NameIndex);
            string HisName = Names[NameIndex];
            Names.RemoveAt(NameIndex);
            foreach (var item in ConnectedClients)
            {
                item.SomeoneLeft(HisName);
            }
        }

        //just for testing the conneXION
        public void SendMessage(string text)
        {
            ICallback callback = OperationContext.Current.GetCallbackChannel<ICallback>();
            int NameIndex = ConnectedClients.IndexOf(callback);
            foreach (var item in ConnectedClients)
            {
                item.SendMess(Names[NameIndex], text);
            }
        }

        public void SendLocation(Point[] location,Point meal)
        {
            ICallback callback = OperationContext.Current.GetCallbackChannel<ICallback>();
            int index = ConnectedClients.IndexOf(callback);
            locations[index] = location;
            meals[index] = meal;
            count++;
            if (count<ConnectedClients.Count)
            {
                return;
            }
            count = 0;
            (new Thread(new ThreadStart(LaunchCallback))).Start();
        }
        private void LaunchCallback()
        {
            foreach (var item in ConnectedClients)
            {
                item.ReceiveLocation(locations, meals);
            }
        }

        
    }
}
