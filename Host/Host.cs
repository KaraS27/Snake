﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ServiceModel;

namespace Host
{
    public partial class Host : Form
    {
        ServiceHost MyHost = null;
        public Host()
        {
            InitializeComponent();
            bStop.Enabled = false;

        }

        private void bLaunch_Click(object sender, EventArgs e)
        {
            MyHost = new ServiceHost(typeof(SnakeService.SnakeService));
            // abc
            MyHost.AddServiceEndpoint(typeof(SnakeContract.ISnakeContract), new NetTcpBinding(), "net.tcp://localhost:8888/MySnakeGame");
            try
            {
                MyHost.Open();
            }
            catch 
            {

                MessageBox.Show("Host Can't ne launcher", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("Host Succesfuly launched...");
            bLaunch.Enabled = false;
            bStop.Enabled = true;
        }

        private void bStop_Click(object sender, EventArgs e)
        {
            try
            {
                MyHost.Close();
            }
            catch 
            {

                MessageBox.Show("Error,Host can't be stopped...", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show("successfully stopped");
            bStop.Enabled = false;
            bLaunch.Enabled = true;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MyHost.State == CommunicationState.Opened)
                {
                    MessageBox.Show("Please stop the host before", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
        }
    }
}
